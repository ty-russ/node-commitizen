const express = require('express');

const app = express();
const port = 3000 || process.env.PORT;
//sophos

app.get("/",(req,res) => {
    res.status(200).json({message:"Okay"})
})
app.get('/endpoint/v1/reports/security_events', (req, res) => {
    res.status(200).json({ data: [
        {
            "name":"log1",
            "records":["logs"] 
        },
        {
            "name":"log2",
            "records":["logs"]
        }
    ] })
})

app.listen(port, () => {
    console.log(`App started on port ${port}`)
});

module.exports = app;