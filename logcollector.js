const request = require('request');

// Sophos Cloud API endpoint and API key
const sophosApiUrl = 'http://127.0.0.1:3000/endpoint/v1/reports/security_events';
const sophosApiKey = '<your_api_key>';

// Wazuh API endpoint and API key
const wazuhApiUrl = 'http://127.0.0.1:4000/api/v3';
const wazuhApiKey = '<your_wazuh_api_key>';

// Headers for the Sophos Cloud API request
const sophosHeaders = {
  'Authorization': 'ApiKey ' + sophosApiKey,
  'Accept': 'application/json'
};

// Headers for the Wazuh API request
const wazuhHeaders = {
  'Authorization': 'Bearer ' + wazuhApiKey,
 'Content-Type': 'application/json'
};

// Send a GET request to the Sophos Cloud API to retrieve the security events
request({
  url: sophosApiUrl,
  headers: sophosHeaders
}, (error, response, body) => {
  if (!error && response.statusCode === 200) {

    // Parse the JSON response from the Sophos Cloud API
    const sophosLogs = JSON.parse(body).data;
    
    // Iterate over the logs and send them to the Wazuh API
    sophosLogs.forEach(async (log) => {
      // Send a POST request to the Wazuh API to send the log'
      let req_body = await JSON.stringify(log)
      //console.log("=====",req_body)
      request({
        method: 'POST',
        url: wazuhApiUrl + '/logs',
        headers: wazuhHeaders,
        body: req_body
      }, (error, response, body) => {

        if (!error && response.statusCode === 201) {
          console.log('Log sent to Wazuh API',body);
        } else {
          console.error('Failed to send log to Wazuh API: ' + body);
        }
      });
    });
  } else {
    console.error('Failed to retrieve logs from Sophos Cloud API: ' + body);
  }
});