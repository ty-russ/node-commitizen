const express = require('express');

const app = express();
app.use(express.json())
const port = 4000 || process.env.PORT;
//wazuh
app.post('/api/v3/logs', (req, res) => {
    console.log(req.body);
    let logs = JSON.parse(JSON.stringify(req.body))

    console.log("recieved logs ===",logs);

    res.status(201).json({ message: 'Success' })
})

app.listen(port, () => {
    console.log(`App started on port ${port}`)
});

module.exports = app;